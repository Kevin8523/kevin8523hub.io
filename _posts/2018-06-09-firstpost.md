---
layout: single
title:  "My First Post"
date:   2018-06-09
---
Hello! I created this blog to record my journey in becoming a Data Scientist and to help others with things I've came across along the way. I plan on using this blog as a personal journal that will do the following:
- Breakdown techincal jargon into simple and easy to understand words (think "Explain like I'm 5")
- Provide tips I've learned throughout my journey (I am a firm believer in learning from those around you)
- Showcase random projects I've work on during my free time
- Give my thoughts & experiences on commonly asked questions
