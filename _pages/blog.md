---
title:  "Archive"
layout: archive
permalink: /Blog/
author_profile: True
comments: True
---
{% for post in site.posts %}
	{% include archive-single.html %}
{% endfor %}