---
title:  "About Me"
layout: archive
permalink: /About/
author_profile: true
comments: true
---

Currently I am working as an Advanced Analytics Analyst at Lockheed Martin. I earned my Bachelors of Science in Mathematics from the Univeristy of Texas at Austin where I majored in Actuarial Science. In my spare time I enjoy working on data related projects and staying active through sports.
- __Email:__ Kevin8523@gmail.com
- __LinkedIn:__ [https://www.linkedin.com/in/khuang3/](https://www.linkedin.com/in/khuang3/)

## Work Experience
- __Advanced Analytics Analyst__, Lockheed Martin, 2017 - Present 
- __Engineering Analyst__, Jones Energy, 2014 - 2016
- __Commercial Underwriting Associate__, Burns & Wilcox, 2013 - 2014

## Education
- __B.S., Mathematics__, University of Texas, Austin,TX. 2012
 
## Technical Skills
- R
- Python
- SQL
- Tableau
- KNIME
- SAP HANA
- Git
