# Framework for doing Data Science in Python

## <a id='0'>O.S.E.M.N </a> 
- <a href='#1'>1. Obtain Data</a>
    - <a href='#1-1'>1.1 Load Dataset</a>
- <a href='#2'>2. Scrub data / Clean Data</a>
    - <a href='#2-1'>2.1 Missing Values</a>
- <a href='#3'>3. Explore Data / Feature Engineer</a>
    - <a href='#3-1'>Explore Dataset - Summary Statistics</a>
    - <a href='#3-2'>Visualize Dataset</a>
    - <a href='#3-3'>Feature Engineering</a>
- <a href='#4'> 4. Model Data</a>		
- <a href='#5'> 5. iNsights</a>       

## <a id='4'>Model Data</a>
    - Process for modeling in Python with sci-kit-learn
		a. Import model class
		b. Instantiate the class and specify the hyper-parameters
		c. Fit to data
		d. Apply/predict to new data 